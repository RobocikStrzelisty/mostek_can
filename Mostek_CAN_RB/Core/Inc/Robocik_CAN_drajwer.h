/*
 * Robocik_CAN_drajwer.h
 *
 *  Created on: 19.02.2020
 *      Author: Dell
 */

#ifndef ROBOCIK_CAN_DRAJWER_H_
#define ROBOCIK_CAN_DRAJWER_H_


/*** Sekcja z inkludesami ***/

// do��czamy bibliotek� CMSIS �eby miec dost�p do tych wszystkich makr
#include "stm32f1xx.h"

/*** Koniec sekcji z inkludesami ***/


/*** Sekcja ze sta�ymi symbolicznymi itp. ***/

typedef enum
{
	Robocik_CAN_TransmitFifoPriority_IdentifierBased,
	Robocik_CAN_TransmitFifoPriority_RequestOrder
}Robocik_CAN_TransmitFifoPriority;

typedef enum
{
	Robocik_CAN_ReceiveFifoLockedMode_OverwriteEnabled,
	Robocik_CAN_ReceiveFifoLockedMode_OverwriteLocked
}Robocik_CAN_ReceiveFifoLockedMode;

typedef enum
{
	Robocik_CAN_NoAutomaticRetransmition_Disabled,
	Robocik_CAN_NoAutomaticRetransmition_Enabled
}Robocik_CAN_NoAutomaticRetransmition;

typedef enum
{
	Robocik_CAN_FilterMode_Mask,
	Robocik_CAN_FilterMode_Identifier
}Robocik_CAN_FilterMode;

typedef enum
{
	Robocik_CAN_FilterScale_Dual,
	Robocik_CAN_FilterScale_Single
}Robocik_CAN_FilterScale;

typedef enum
{
	Robocik_CAN_FilterFIFOAssigment_FIFO0,
	Robocik_CAN_FilterFIFOAssigment_FIFO1
}Robocik_CAN_FilterFIFOAssigment;

typedef enum
{
	Robocik_CAN_TransmitState_Success,
	Robocik_CAN_TransmitState_ArbitrationLost,
	Robocik_CAN_TransmitState_TransmissionErrorDetection,
	Robocik_CAN_TransmitState_NoEmptyMailboxes,
	Robocik_CAN_TransmitState_StrangeErrorXD
}Robocik_CAN_TransmitState;

typedef enum
{
	Robocik_CAN_ReceiveState_Success,
	Robocik_CAN_ReceiveState_NoPendingMessages,

	Robocik_CAN_ReceiveState_StrangeErrorXD
}Robocik_CAN_ReceiveState;

#define Robocik_CAN_BAZA 0x40000000 // baza rejestr�w peryferi�w
#define Robocik_CAN_BAZA_ALIASU 0x42000000 // baza aliasow

/*** Koniec sekcji ze sta�ymi symbolicznymi itp. ***/


/*** Sekcja z makrami ***/

#define Robocik_CAN_BB(adresRejestru,  ktoryBit) (* (volatile uint32_t *)(Robocik_CAN_BAZA_ALIASU + (((uint32_t)&adresRejestru) - Robocik_CAN_BAZA) * 32 + (ktoryBit) * 4))

/*** Koniec sekcji z makrami ***/

/*** Sekcja z definicjami struktur ***/
// W strukturach trzeba rozwa�yc u�ycie p�l bitowych

// Struktura s�u��ca inicjalizacji CANa, przeznaczona do wys�ania funkcji inicjalizacyjnej
typedef struct
{
	// Nie pisz - wolne miejsca, s� po to by u�atwic mi prac�
	uint32_t	niepisz3:2,
	// Transmit FIFO priority - je�li 1, to priorytet wysy�ania wiadomo�ci b�dzie
	//wyznaczony przez kolejno�c ich nadawania, je�li 0 to przez warto�c identyfikatora
	// (im mniejszy tym szybciej)
				CAN_TransmitFifoPriority:1,
	// Receive FIFO locked mode - je�li 1 to gdy FIFO jest pe�ne a nadejdzie kolejna wiadomo�c,
	// to zostanie ona porzucona, je�li 0 to nadpisze ostatni� wiadomo�c w FIFO
				CAN_ReceiveFifoLockedMode:1,
	// No automatic retransmission - je�li 1 to wiadomo�c b�dzie wysy�ana tylko raz
	// niezale�nie od tego, czy transmisja zako�czy si� powodzeniem np. przez przegrany arbitra�
	// albo b��d
				CAN_NoAutomaticRetransmition:1,
	// Automatic wakeup mode - je�li 1 to przy otrzymaniu wiadomo�ci kontroler samoistnie
	// wyjdzie ze stanu u�pienia
				CAN_AutomaticWakeUpMode:1,
	// Automatic bus-off managment - je�li jeden, to kontroler samoistnie podejmuje dzia�anie
	// by wyj�c ze stanu odci�cia od linii
				CAN_AutomaticBusOfManagment:1,
	// Time triggered communication mode - je�li 1 to kontroler nak�ada stemple czasowe
	// na wiadomo�ci zar�wno wysy�ane jak i odbierane
				CAN_TimeTriggeredCommunicationMode:1,

				niepisz2:8,
	// TUTAJ JESZCZE JEST MIEJSCE NA JEDEN BIT KONFIGURACYJNY DOTYCZACY SOFTWAROWEGO RESETU

	// Debug freeze - je�li 1 to odbieranie i wysy�anie jest zamro�one na czas debugu
				CAN_DebugFreeze:1,

				niepisz1:15;

	// Baud rate prescaler - 10 bit�w ustawiaj�cych dzielnik sygna�u zegarowego do kontrolera
	uint32_t	CAN_BaudRatePrescaler:10,

				niepisz6:6,
	// Time segment 1 - 4 bity definiuj� ilo�c kwant�w czasu w pierwszym segmencie czasu - bit
	// tajming jest dok�adniej opisane w manualu
				CAN_TimeSegment1:4,

	// Time segment 2 - 3 bity, tak jak opisano wy�ej
				CAN_TimeSegment2:3,

				niepisz5:1,
	// Resynchronization time width - dwa bity okre�laj�ce o ile kwant�w czasu kontroler mo�e
	// wyd�u�yc lub skr�cic czas pr�bkowania jednego bitu
				//CAN_SJW:2,
				CAN_ResynchronizationTimeWidth:2,

				niepisz4:4,
	// Loop back mode - je�li 1 to kontroler b�dzie dzia�a� w trybie p�tli powrotnej,
	// mo�na ��czyc z trybem cichym
				CAN_LoopbackMode:1,

	// Silent mode - je�li 1 to kontroler b�dzie dzia�a� w trybie cichym
				CAN_SilentMode:1;
}Robocik_CAN_InitTypeDef;


typedef union
{
	// S�owo na standardowy identyfikator - 11bitowy, je�li u�ywany jest rozszerzony
	// to zostawic
	uint32_t stdId;
	// S�owo na rozszerzony identyfikator - 29bitowy, je�li nieu�ywany, zostawic
	uint32_t extId;
}Robocik_CAN_IdUnion;



// Struktura zawieraj�ce dane pobierane przez skrzynk� nadawcz� przed wys�aniem,
// zawiera oczywi�cie wspomniane dane, ale r�wnie� identyfikator, to ile bit�w
// niesie wiadomo�c i jeszcze par� innych pierd�, opisanych w tre�ci definicji
typedef struct
{
	// Transmit mailbox request - je�li 1 to wysy�amy ��danie wys�ania
	// W zasadzie wstawiam to nie do nadpisywania, bo w funkcji b�d� i tak zak�ada�,
	// �e r�wna si� 1, a po to, by mi si� �adnie bity wyr�wna�y i mo�na by�o to
	// elegancko zapisac do rejestru
	uint32_t TransmitMailboxRequest:1,
	// Remote transmission request - je�li 1 to mamy do czynienia z ramk�
	// ��dania transmisji
			 RemoteTransmissionRequest:1,
	// Identifier extension - je�li 1 to oznacza, �e mamy do czynienia z rozszerzonym
	// identyfikatorem
			 IdentifierExtention:1,

	// S�owo na rozszerzony identyfikator - 29bitowy, je�li nieu�ywany, zostawic
			 extId:18,

	// S�owo na standardowy identyfikator - 11bitowy, je�li u�ywany jest rozszerzony
	// to zostawic
			 stdId:11;
	// Data length code - 4 bity okre�laj�ce, jak d�ug� wiadomo�c wysy�amy/otrzymujemy
	uint32_t DLC:4,
	// przesz�a dana wiadomo�c, tyczy si� tylko wiadomo�ci przychodz�cych
			 niepisz:4,
	// Filter match index - 8 bit�w przechowuj�cych informacj�, przez kt�ry filtr
			 FilterMatchIndex:8,
	// Przy wysylaniu liczy sie tylko najmlodszy bit, ktory nazywa sie wtedy
	// Transmit Global Time i okresla, czy jest wysylany stempel czasowy (1 jesli tak);
	// jesli jest wysylany, to zajmuje on dwa ostanie bajty wiadomosci (bajt 7 i 6),
	// w tej sytuacji DLC zawsze musi byc ustawione na 8, bo zawsze wykorzystujemy 8 bajtow.
	// JEDNAKZE - jest to istotne tylko w sytucji, gdy wlaczylismy Time Trigger Communication
	// Mode

	// Miejsce na stempel czasowy
			 TIME:16;
	// Data - o�miobajtowa tablica na dane
	uint8_t data[8];


}Robocik_CAN_DataFrame;


// struktura u�ywana przez funkcje inicjalizuj�ce filtry; filtry s� inicjalizowane
// pojedy�czo, nie zbiorczo
typedef struct
{
	// Numer inicjalizowanego filtru - przypominam, w f103 jest ich 14
	uint8_t filterNumber:4,
	// Filter mode - tryb filtru, je�li 0 to filtr b�dzie dzia�a� w trybie maski,
	// tzn. pierwsze ze s��w filtru (a ka�dy filtr sk�ada si� z dw�ch s��w) b�dzie
	// stanowi�o mask� bitow�, okre�laj�c�, na kt�rych bitach adresu nam zale�y,
	// a kt�re olewamy, za� je�li 1 to oba s�owa b�d� stanowi�y jakby dwa oddzielne filtry
			filterMode:1,
	// Filter scale - skala (?XD) maski - je�li 0 to ka�de s�owo filtra zostanie podzielone
	// na dwa 16bitowe pod rejestry stanowi�ce dwa oddzielne filtry, je�li 1 to b�dziemy
	// mieli 2 32bitowe s�owa
			filterScale:1,
	// Przypisanie filtru odpowiedniemu FIFO - je�li 1 to FIFO1, je�li 0 to FIFO0
			filterFIFOAssigment:1,
	// Aktywacja filtra - je�li 1, to w trakcie inicjalizacji filtr zostanie
	// od razu aktywowany, je�li 0 to pozostanie nieaktywny i b�dzie nale�a�o
	// ewentualnie u�yc funkcji go aktywuj�cej
			filterActivation:1;

	// nast�pnie kolejne p�s�owa s�owa filtru odpowiadaj�cego za Id i Mask�
	uint16_t filterIdHigh;
	uint16_t filterIdLow;
	// WAZNE - jesli nie korzystamy z trybu maski, to jesli chcemy wykorzystac drugi
	// filtr, to nalezy go zainicjalizowac jednoczesnie z pierwszym
	uint16_t filterMaskHigh;
	uint16_t filterMaskLow;

}Robocik_CAN_FilterInitTypeDef;

/*** Koniec sekcji z definicjami struktur ***/


/*** Sekcja z deklaracjami funkcji ***/
// Mo�na pomy�lec o wdro�eniu jakiej� procedury obs�ugi b��du, ale to ew. potem

	/* Poni�ej funkcje uruchamiaj�ce i konfiguruj�ce kontroler - bez przerwa� */

// Co robi:		funkcja inicjalizuje kontroler w podstawowy spos�b, wraz z przerwaniem
//				generowanym przy otrzymaniu danych przez FIFO
// Co pobiera:	nic
// Co zwraca:	je�li inicjalizacja przebieg�a pomy�lnie SUCCESS, je�li nie ERROR
ErrorStatus Robocik_CAN_BasicInit(void);

// Co robi: 	funkcja inicjalizacyjna kontrolera bxCAN w STM32F103, a dok�adniej
//				przechodzi w tryb inicjalizacji, ustawia rejestr kontrolny MCR
//				oraz rejestr bittajmingu BTR; dok�adnie dzia�anie b�dzie wyja�nione
//				na przyk�adzie i mo�e w jakim� kr�tkim tutorialu
// Co pobiera:	pobiera struktur� inicjalizacyjn� kontrolera CAN, kt�ra jest opisana
//				powy�ej
// Co zwraca:	je�li inicjalizacja przebieg�a pomy�lnie SUCCESS, je�li nie ERROR
ErrorStatus Robocik_CAN_Init(Robocik_CAN_InitTypeDef * initStruct);

// Co robi:		uruchamia kontroler
// Co pobiera:	nic
// Co zwraca:	nic
void Robocik_CAN_EnableNormalMode(void);

// Co robi:		wchodzi w tryb u�pienia
// Co pobiera:	nic
// Co zwraca:	nic
void Robocik_CAN_EnableSleepMode(void);

// Co robi:		konfiguruje i w��cza okre�lony w strukturze inicjalizacyjnej filtr
// Co pobiera:	struktur� inicjalizacyjn� opisan� powy�ej
// Co zwraca:	status b��du
ErrorStatus Robocik_CAN_FilterInit(Robocik_CAN_FilterInitTypeDef *initStruct);

// Co robi:		aktywuje odpowiedni filtr
// Co pobiera:	numer filtru (od zera oczywi�cie)
// Co zwraca:	nic
void Robocik_CAN_EnableFilter(uint8_t whichFilter);

		/* Poni�ej definicje funkcji uruchamiaj�cych i wy��czaj�cych przerwania */

// Co robi:		w��cza przerwanie generowane przy otrzymaniu wiadomo�ci przez
//				okre�lone na wej�ciu FIFO, co wa�ne - obs�ug� przerwania trzeba jeszcze
//				potem w��czyc w NVICu - NVIC_EnableIRQ(CAN1_RX1_IRQn);
// Co pobiera:	pobiera pozycj� bitu, kt�ry odpowiada za dane przerwanie, bity te s�
//				opisane w CMSIS nast�puj�co: CAN_IER_FMPIEx, gdzie ko�cowa cyfra
//				mo�e wynosic 0 lub 1, zale�nie od tego, kt�re FIFO ma generowac przerwanie
// Co zwraca:	nic
void Robocik_CAN_EnableIT_FifoMessagePendingIT(uint8_t whichFIFO);

		/* Poni�ej deklaracje funckji zwi�zanych z wysy�aniem */

// Co robi:		wysy�a ramk� danych
// Co pobiera:	ramk� danych, kt�r� jest struktura opisana powy�ej
// Co zwraca:	je�li transmisja jest udana - SUCCESS, je�li przegrano arbitra� - arbitration
//				lost, je�li wyst�pi� b��d transmisji - transmision error detection
//				typ (enumeracyjny) stanu transmisji zosta�a opisana powy�ej, je�li wszystkie
//				skrzynki s� zaj�te - NoEmptyMailboxes
Robocik_CAN_TransmitState Robocik_CAN_Transmit(Robocik_CAN_DataFrame * dataFrame);


		/* Poni�ej deklaracje funkcji zwi�zanych z odbieraniem */

// Co robi:		odbiera ramk� danych z okre�lonego FIFO (mo�e byc 0 lub 1)
//				i zeruje flag� RFOMwyzwalaj�c miejsce w FIFO
// Co pobiera:	wska�nik na ramk� danych oraz numer fifo z kt�rego odbieramy dane
// Co zwraca:	SUCCESS je�li operacja si� powiedzie, ERROR je�li nie lub nie ma danych
//				do pobrania
Robocik_CAN_ReceiveState Robocik_CAN_Receive(Robocik_CAN_DataFrame * dataFrame, uint8_t whichFIFO);


		/* Poni�ej deklaracje funkcji zwracaj�cych okre�lone stany kontrolera */


/*** Koniec sekcji z deklaracjami funkcji ***/


#endif /* ROBOCIK_CAN_DRAJWER_H_ */

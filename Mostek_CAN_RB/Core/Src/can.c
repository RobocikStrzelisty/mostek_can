/*
 * can.c
 *
 *  Created on: Mar 4, 2021
 *      Author: Dell
 */

#include "can.h"

void CAN_init(void)
{
	// Konfiguracja CANa
	Robocik_CAN_InitTypeDef initTest = {0};
	initTest.CAN_NoAutomaticRetransmition = ENABLE;
	initTest.CAN_LoopbackMode = DISABLE;
	initTest.CAN_ResynchronizationTimeWidth = 1;
	initTest.CAN_TimeSegment2 = 2;
	initTest.CAN_TimeSegment1 = 13;
	initTest.CAN_BaudRatePrescaler = 5;
	Robocik_CAN_Init(&initTest);

	// Wlaczamy przerwanie od wiadomosci przychodzacych do fifo0
	//Robocik_CAN_EnableIT_FifoMessagePendingIT(0);

	//NVIC_EnableIRQ(USB_LP_CAN1_RX0_IRQn);
}

void CAN_filter_init(void)
{
	Robocik_CAN_FilterInitTypeDef initFilter = {0};
		// Adres wpisywany w filtr - wykorzystujemy adres krotki (11 bitow),
		// wiec wpisujemy adres w miejsce na adresy skrocone (gorne polslowo, najstarsze 11 bitow)
		// Do gornego polslowa dodano jeszcze 7, w miejsce na adres rozszerzony,
		// zeby zademonstrowac brak wplywu tych bitow na rozpoznanie wiadomosci o skroconym adresie
	initFilter.filterIdHigh = (0x650U << (16U - 11U));
	initFilter.filterMaskLow = 0x123U;
	initFilter.filterNumber = 0;
	initFilter.filterActivation = ENABLE;
	initFilter.filterMode = Robocik_CAN_FilterMode_Identifier;
	initFilter.filterScale = Robocik_CAN_FilterScale_Single;
	initFilter.filterFIFOAssigment = 0;
	Robocik_CAN_FilterInit(&initFilter);
}

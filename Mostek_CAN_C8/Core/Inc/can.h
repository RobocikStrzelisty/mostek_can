/*
 * can.h
 *
 *  Created on: Mar 4, 2021
 *      Author: Dell
 */

#ifndef INC_CAN_H_
#define INC_CAN_H_

#include "Robocik_CAN_drajwer.h"

void CAN_init(void);
void CAN_filter_init(void);

#endif /* INC_CAN_H_ */

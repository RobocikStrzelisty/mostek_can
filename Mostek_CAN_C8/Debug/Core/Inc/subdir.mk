################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Inc/Robocik_CAN_drajwer.c 

OBJS += \
./Core/Inc/Robocik_CAN_drajwer.o 

C_DEPS += \
./Core/Inc/Robocik_CAN_drajwer.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Inc/Robocik_CAN_drajwer.o: ../Core/Inc/Robocik_CAN_drajwer.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 '-DHSE_VALUE=8000000' -DSTM32F103xB '-DHSE_STARTUP_TIMEOUT=100' -DDEBUG '-DLSE_STARTUP_TIMEOUT=5000' '-DLSE_VALUE=32768' '-DVDD_VALUE=3300' '-DLSI_VALUE=40000' '-DHSI_VALUE=8000000' -DUSE_FULL_LL_DRIVER '-DPREFETCH_ENABLE=1' -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Inc/Robocik_CAN_drajwer.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"


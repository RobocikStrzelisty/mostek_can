# Mostek CAN między dwoma STM32F103
Jest to krótka prezentacja działania CANa na mikrokontrolerach STM32. Repo zawiera programy dla dwóch STMek (F103RB i C8), które realizują funkcję mostka CAN, tzn. pierwsza z nich odbiera dane przez UART2 z komputera, które następnie są przesyłane po magistrali CAN do drugiej. Ta zaś, korzystając z UARTa1, przesyła je do komputera.
